# Stage 1: Intermediate stage to copy artifact files
FROM alpine:latest as intermediate

# Create a directory for artifact files
RUN mkdir /artifacts

# Copy artifact files from the previous stages into the intermediate image
COPY build_result.txt /artifacts/
COPY test_result.txt /artifacts/
COPY package_result.txt /artifacts/

# Stage 2: Final image
FROM alpine:latest

# Copy artifact files from the intermediate stage into the final image
COPY --from=intermediate /artifacts /artifacts